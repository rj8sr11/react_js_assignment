import React, { useEffect, useState } from "react";
import "./App.css";
import api from "./api/commonUrl";
import { v4 as uuid } from "uuid";

import { Routes, Route } from "react-router-dom";
const AddBook = React.lazy(() => import("./Component/AddBook"));
const Home = React.lazy(() => import("./Component/Home"));
const Login = React.lazy(() => import("./Component/LoginPage"));
const BooksList = React.lazy(() => import("./Component/BooksList"));
const DeleteBook = React.lazy(() => import("./Component/DeleteBook"));
const UpdateBookDetails = React.lazy(() => import("./Component/UpdateBook"));

function App() {
  const [books, setBooks] = useState([]);

  const retrieveAllBookDetails = async () => {
    const response = await api.get("/books");
    return response.data;
  };

  useEffect(() => {
    const getAllBooks = async () => {
      const allBooks = await retrieveAllBookDetails();
      if (allBooks) {setBooks(allBooks)}
    };
    getAllBooks();
  }, []);

  const addBook = async (book) => {
    console.log(book);
    const request = {
      id: uuid(),
      ...book,
    };
    const response = await api.post("/books", request);
    console.log(response);
    setBooks([book, response.data]);
  };
  // useEffect(() => {}, [books]);

  return (
    <>
      <React.Suspense fallback={<span>Loading...</span>}>
        <Routes>
          <Route exact path="/" element={<Login />} />
          <Route exact path="/welcome" element={<Home />} />
          <Route
            exact
            path="/add/book"
            element={<AddBook addBook={addBook} />}
          />
          <Route
            exact
            path="/book/list"
            element={<BooksList books={books} />}
          />
          <Route
            exact
            path="/delete/book"
            element={<DeleteBook books={books} />}
          />
          <Route exact path="/update/author/name" element={<Home />} />
          <Route
            exact
            path="/update/book/detail"
            element={<UpdateBookDetails />}
          />
          <Route exact path="/logout" element={<Login />} />
          <Route
            path="*"
            element={
              <main style={{ padding: "1rem" }}>
                <p>There's nothing here!</p>
              </main>
            }
          />
        </Routes>
      </React.Suspense>
    </>
  );
}

export default App;
