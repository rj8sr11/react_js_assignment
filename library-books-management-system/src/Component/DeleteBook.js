import React, { useState } from "react";
import NavBar from "./NavBar";

export default function DeleteBook(props) {
  // const booksDetails = props.books.map((bookDetail, index) => {
  //   return (
  //     <div className="container" key={index}>
  //       Book Name : {bookDetail.bookName}
  //       Author Name: {bookDetail.authorName}
  //       Book Edition : {bookDetail.bookEdition}
  //       Book Price:${bookDetail.bookPrice}
  //       Customer Name:{bookDetail.customerName}
  //     </div>
  //   );
  // });
  const [text, setText] = useState("");

  const search = props.books
    .filter((book) => {
      if (book.bookName.includes(text)) {
        return book;
      } else if (text === " ") {
        return book;
      } else return "";
    })
    .map((book, index) => (
      <div className="container" key={index}>
        Book Name : {book.bookName}
        Author Name: {book.authorName}
        Book Edition : {book.bookEdition}
        Book Price:${book.bookPrice}
        Customer Name:{book.customerName}
      </div>
    ));

  return (
    <>
      <NavBar />
      <input
        type="text"
        id="header-search"
        placeholder="Search by book name"
        value={text}
        onChange={(e) => {
          setText(e.target.value);
        }}
      />
      {/* {booksDetails} */}
      <div className="container">{search}</div>
    </>
  );
}
