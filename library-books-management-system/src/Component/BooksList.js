import React, { useEffect } from "react";
import NavBar from "./NavBar";
export default function BooksList(props) {
  useEffect((state) => {
    document.title = "Book List";
  });
  console.log(props);
  const booksDetails =props.books.map((bookDetail, index) => {
    return (
      <div className="container" key={index}>
        Book Name : {bookDetail.bookName}
        Author Name: {bookDetail.authorName}
        Book Edition : {bookDetail.bookEdition}
        Book Price:${bookDetail.bookPrice}
        Customer Name:{bookDetail.customerName}
      </div>
    );
  });

  return (
    <>
      <NavBar />
      {booksDetails}
      </>
  );
}
