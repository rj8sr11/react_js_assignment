import React, { useEffect, useState } from "react";
import NavBar from "./NavBar";
//import { useNavigate } from "react-router-dom";

export default function AddBook(props) {
  useEffect(() => {
    document.title = "Add Book";
  });
  // const navigate = useNavigate();
  const [bookName, setBookName] = useState("");
  const [authorName, setAuthorName] = useState("");
  const [bookEdition, setBookEdition] = useState("");
  const [bookPrice, setBookPrice] = useState("");
  const [customerName, setCustomerName] = useState("");
  const state = {
    bookName: bookName,
    authorName: authorName,
    bookEdition: bookEdition,
    bookPrice: bookPrice,
    customerName: customerName,
  };
  return (
    <>
      <NavBar />
      <div
        className="container"
        style={{ textAlign: "center", marginTop: "4em" }}
      >
        <form
          onSubmit={(e) => {
            e.preventDefault();
            if (bookName !== null && bookName.length > 0) {
              props.addBook(state);
              setBookName("");
              setAuthorName("");
              setBookEdition("");
              setBookPrice("");
              setCustomerName("");
              alert("book details submitted successfully");
              // navigate("/book/list");
            } else {
              alert("values cant be empty");
            }
          }}
        >
          <div className="form-row">
            <div className="form-group col-md-6">
              <label htmlFor="inputBookName">Book Name</label>
              <input
                value={bookName}
                type="text"
                className="form-control"
                id="bookname"
                placeholder="Enter Book Name"
                onChange={(e) => {
                  setBookName(e.target.value);
                }}
              />
            </div>
            <div className="form-group col-md-6">
              <label htmlFor="inputAuthorName">Author Name</label>
              <input
                value={authorName}
                type="text"
                className="form-control"
                id="authorname"
                placeholder="Enter Author Name"
                onChange={(e) => {
                  setAuthorName(e.target.value);
                }}
              />
            </div>
          </div>
          <div className="form-group col-md-6">
            <label htmlFor="inputBookEdition">Book Edition</label>
            <input
              value={bookEdition}
              type="text"
              className="form-control"
              id="bookedition"
              placeholder="Enter Book Edition"
              onChange={(e) => {
                setBookEdition(e.target.value);
              }}
            />
          </div>
          <div className="form-group col-md-6">
            <label htmlFor="inputBookPrice">Book Price</label>
            <input
              value={bookPrice}
              type="text"
              className="form-control"
              id="bookprice"
              placeholder="Enter Book Price"
              onChange={(e) => {
                setBookPrice(e.target.value);
              }}
            />
          </div>
          <div className="form-row">
            <div className="form-group col-md-6">
              <label htmlFor="inputCustomerName">Customer Name</label>
              <input
                value={customerName}
                type="text"
                className="form-control"
                id="customername"
                placeholder="Enter Customer Name"
                onChange={(e) => {
                  setCustomerName(e.target.value);
                }}
              />
            </div>
          </div>
          <div className="form-group col-md-6" style={{ marginTop: "1em" }}>
            <button type="submit" className="btn btn-primary">
              Add Book
            </button>
          </div>
        </form>
      </div>
    </>
  );
}
