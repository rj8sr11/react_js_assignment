import React from "react";
import { Link } from "react-router-dom";
export default function NavBar() {
  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container-fluid">
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <Link
                  className="nav-link active"
                  aria-current="page"
                  to="/add/book"
                >
                  Add Book
                </Link>
              </li>
              <li className="nav-item" style={{ marginLeft: "10em" }}>
                <Link
                  className="nav-link active"
                  aria-current="page"
                  to="/book/list"
                >
                  Book List
                </Link>
              </li>
              <li className="nav-item" style={{ marginLeft: "10em" }}>
                <Link className="nav-link" to="/delete/book">
                  Delete Book
                </Link>
              </li>
              <li className="nav-item" style={{ marginLeft: "10em" }}>
                <Link className="nav-link" to="/update/book/detail">
                  Update Book Details
                </Link>
              </li>
              {/* <li className="nav-item" style={{ marginLeft: "10em" }}>
                <Link className="nav-link" to="/update/customer/detail">
                  Update Customer Detail
                </Link>
              </li> */}
              <li className="nav-item" style={{ marginLeft: "10em" }}>
                <Link className="nav-link" to="/logout">
                  Logout
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
}
