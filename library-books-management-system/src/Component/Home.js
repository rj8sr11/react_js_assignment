import React, { useEffect } from "react";
import NavBar from "./NavBar";
export default function Home(props) {
  useEffect(() => {
    document.title = "Home";
  });
  return (
    <>
      <NavBar />
      <div className="App"></div>
    </>
  );
}
